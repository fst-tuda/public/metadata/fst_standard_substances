# This is the readme of the **oxygen** substance
**NOTE: Please cite https://doi.org/10.18434/T4D303  if you use this data set in your research like the following, thank you. :**

</br>
"""

Eric W. Lemmon, Ian H. Bell, Marcia L. Huber, and Mark O. McLinden, "Thermophysical Properties of Fluid Systems" in <b>NIST Chemistry WebBook, NIST Standard Reference Database Number 69</b>, Eds. P.J. Linstrom and W.G. Mallard, National Institute of Standards and Technology, Gaithersburg MD, 20899, https://doi.org/10.18434/T4D303,   (retrieved March 23, 2024).

"""
