## Sensor None

<div align="right">

### IRI: [`https://w3id.org/fst/resource/064f05d1-5d2b-7507-8000-522c3584927c`](https://w3id.org/fst/resource/064f05d1-5d2b-7507-8000-522c3584927c)
### UUID: `064f05d1-5d2b-7507-8000-522c3584927c`
### identifier: `FST-INV:PUR-foam-PPI60`

</div>

## Keywords: 

## General Info

| property | value |
|-:|:-|
| comment: | 
Polyurethan foam on polyester basis. pore size structure open and regular |
| manufacturer: | EMW Filtertechnik GmbH |
| name: | None |
| serial number: | None |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Lefemmine |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, https://schema.org/ChemicalSubstance |

