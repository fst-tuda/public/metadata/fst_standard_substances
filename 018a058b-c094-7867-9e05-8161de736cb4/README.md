## Sensor None

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018a058b-c094-7867-9e05-8161de736cb4`](https://w3id.org/fst/resource/018a058b-c094-7867-9e05-8161de736cb4)
### UUID: `018a058b-c094-7867-9e05-8161de736cb4`
### identifier: `FST-INV:PUR-foam-PPI20`

</div>

## Keywords: 

## General Info

| property | value |
|-:|:-|
| comment: | 
Polyurethan foam on polyester basis. pore size structure open and regular |
| manufacturer: | EMW Filtertechnik GmbH |
| name: | None |
| serial number: | None |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Lefemmine |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, https://schema.org/ChemicalSubstance |

