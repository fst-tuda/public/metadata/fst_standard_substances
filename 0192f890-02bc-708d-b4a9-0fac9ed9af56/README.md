## Sensor None

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0192f890-02bc-708d-b4a9-0fac9ed9af56`](https://w3id.org/fst/resource/0192f890-02bc-708d-b4a9-0fac9ed9af56)
### UUID: `0192f890-02bc-708d-b4a9-0fac9ed9af56`
### identifier: `FST-INV:PUR-foam-PPI40`

</div>

## Keywords: 

## General Info

| property | value |
|-:|:-|
| comment: | 
Polyurethan foam on polyester basis. pore size structure open and regular |
| manufacturer: | EMW Filtertechnik GmbH |
| name: | None |
| serial number: | None |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Lefemmine |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject, https://schema.org/ChemicalSubstance |

